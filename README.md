# vue_json_crud

# 项目说明

本项目采用 Vue+ElmentUI 实现了对于学生信息的增删改查操作，操作的是 json 格式静态数据。

# 项目截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0704/103802_fba87ebd_1505145.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0704/103814_723addf3_1505145.png "2.png")

# 部署步骤

## 安装

```
npm install
```

### 启动

```
npm run serve
```

访问地址：localhost:8080

用户名：admin 密码：123456

### 打包

```
npm run build
```

# 我的公众号

持续分享 Java 全栈干货，欢迎关注 :cn:

![输入图片说明](https://images.gitee.com/uploads/images/2020/1219/124634_a76196f9_1505145.jpeg "qrcode_for_gh_7ed14c667071_258.jpg")
