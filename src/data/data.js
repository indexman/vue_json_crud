let studentList = [
  {
    id: 1,
    name: "小明",
    age: 18,
    studentNo: "12222222",
    major: "专业",
    sex: "男",
  },
  {
    id: 2,
    name: "小红",
    age: 17,
    studentNo: "12222222",
    major: "专业",
    sex: "女",
  },
];

let scoreList = [
  {
    english: 50,
    math: 90,
    studentId: 1,
  },
  {
    english: 80,
    math: 80,
    studentId: 2,
  },
  {
    english: 100,
    math: 99,
    studentId: 3,
  },
];

let userList = [
  {
    name: "admin",
    password: "123456",
  },
];

export { studentList, scoreList, userList };
